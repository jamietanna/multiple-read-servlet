# Multiple Reads with HTTP Servlets in Java

A library to provide the ability to read an HTTP servlet request body multiple times, as mentioned in https://www.jvt.me/posts/2020/05/25/read-servlet-request-body-multiple/.

## Versioning

Note that this project's version matches the `javax.servlet:javax.servlet-api` version, for ease of consumption.

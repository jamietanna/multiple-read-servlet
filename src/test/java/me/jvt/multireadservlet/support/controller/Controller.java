package me.jvt.multireadservlet.support.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
  @PostMapping("/")
  public ResponseEntity<String> echo(@RequestBody String body) {
    return new ResponseEntity<>(body, HttpStatus.OK);
  }
}

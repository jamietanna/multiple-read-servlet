package me.jvt.multireadservlet.support.servlet;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import me.jvt.multireadservlet.MultiReadHttpServletRequest;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class BodyReadFilter implements Filter {

  private static final Logger LOGGER = LoggerFactory.getLogger(BodyReadFilter.class);

  public void doFilter(
      ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
      throws IOException, ServletException {
    MultiReadHttpServletRequest wrappedRequest =
        new MultiReadHttpServletRequest((HttpServletRequest) servletRequest);
    LOGGER.info(
        "The body of the request was {}", IOUtils.toString(wrappedRequest.getInputStream()));
    filterChain.doFilter(wrappedRequest, servletResponse);
  }
}

package me.jvt.multireadservlet.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static uk.org.lidalia.slf4jtest.LoggingEvent.info;

import me.jvt.multireadservlet.support.Application;
import me.jvt.multireadservlet.support.controller.Controller;
import me.jvt.multireadservlet.support.servlet.BodyReadFilter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import uk.org.lidalia.slf4jtest.TestLogger;
import uk.org.lidalia.slf4jtest.TestLoggerFactory;

@ExtendWith(SpringExtension.class)
@WebMvcTest(Controller.class)
@ContextConfiguration(classes = Application.class)
class ControllerIntegrationTest {
  private final TestLogger logger = TestLoggerFactory.getTestLogger(BodyReadFilter.class);

  @Autowired private MockMvc mockMvc;

  @Test
  void itReturnsBodyOfRequest() throws Exception {
    // given

    // when
    this.mockMvc
        .perform((post("/").content("This is the request body")))
        // then
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(content().string("This is the request body"));
  }

  @Test
  void itLogsBody() throws Exception {
    // given

    // when
    this.mockMvc.perform((post("/").content("This is the request body")));

    // then
    assertThat(logger.getLoggingEvents())
        .contains(info("The body of the request was {}", "This is the request body"));
  }
}
